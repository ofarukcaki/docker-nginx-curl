FROM alpine:latest

# install curl
RUN apk --no-cache add curl

# install nginx
RUN apk --no-cache add nginx && \
    mkdir -p /run/nginx

# start nginx server
CMD ["nginx", "-g", "daemon off;"]
